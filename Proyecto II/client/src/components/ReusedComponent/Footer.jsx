import React from "react";
import "./css/Footer.css";
import linkedicon from "./img/linkedicon.ico";
export default function Footer() {
  return (
    <div className="footer border d-flex flex-column justify-content-center align-items-center ">
      <p className="m-0">Jose Luis Sequeira Gongora &copy; 2021</p>
    </div>
  );
}
