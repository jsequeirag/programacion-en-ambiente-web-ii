import { useQuery, gql } from "@apollo/client";
import { useEffect, React, useState } from "react";
import axios from "axios";
import { div } from "prelude-ls";
const news = gql`
  {
    getNews(userid: "61104d7ae96c8682e811b053") {
      userid
      resourceurl
      title
      description
      source
      creator
      link
      itemcategory
      resourcecategory
      image
      date
    }
  }
`;
export default function Home() {
  const [news, setNews] = useState([]);
  /*const { data, loading, error } = useQuery(news);*/
  useEffect(async () => {
    const data = await axios({
      url: "http://localhost:3003/graphql",
      method: "post",
      data: {
        query: `
        {
            getNews(userid:"61104d7ae96c8682e811b053") {
                userid
                resourceurl
                title
                description
                source
                creator
                link
                itemcategory
                resourcecategory
                image
                date
              }
              
            }
          `,
      },
    }).then((r) => {
      console.log(r.data.data.getNews);
      setNews(r.data.data.getNews);
    });
  }, []);
  /*
  return loading === true ? (
    <h1>loading...</h1>
  ) : (
    <div>
      {data.getNews.map((element, key) => (
        <h2 key={key}>{element.userid}</h2>
      ))}
    </div>
  );*/
  return news.length === 0 ? (
    <h1>loading...</h1>
  ) : (
    <div>
      {news.map((element, key) => (
        <h2 key={key}>{element.userid}</h2>
      ))}
    </div>
  );
}
