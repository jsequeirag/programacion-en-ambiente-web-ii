/* -------------------------------------------------------------------------- */
/*                                   package                                  */
/* -------------------------------------------------------------------------- */
const { Router } = require("express");
const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
require("dotenv").config();
/* -------------------------------------------------------------------------- */
/*                                    Model                                   */
/* -------------------------------------------------------------------------- */
const categoriesModel = require("../Model/caterories");

/* -------------------------------------------------------------------------- */
/*                               get categories                               */
/* -------------------------------------------------------------------------- */
router.post("/getcategories", async (req, res) => {
  const categories = categoriesModel.find({}, (err, result) => {
    res.json(result);
  });
});
/* -------------------------------------------------------------------------- */
/*                               delete category                              */
/* -------------------------------------------------------------------------- */
router.post("/deletecategory", async (req, res) => {
  idCategory = req.body.idCategory;
  console.log(idCategory);
  const categories = await categoriesModel.deleteOne(
    { _id: idCategory },
    (err, result) => {
      if (err) {
        res.send("error");
      } else {
        res.send("category deleted");
      }
    }
  );
});
/* -------------------------------------------------------------------------- */
/*                                   insert                                   */
/* -------------------------------------------------------------------------- */
router.post("/insertcategory", async (req, res) => {
  const name = req.body.name;
  const model = new categoriesModel({
    name: name,
  });
  model.save();
  res.send(model);
});
/* -------------------------------------------------------------------------- */
/*                                   update                                   */
/* -------------------------------------------------------------------------- */
router.post("/updatecategory", async (req, res) => {
  const idcategory = req.body._id;
  const nameCategory = req.body.name;
  const newCategory = {
    _id: idcategory,
    name: nameCategory,
  };
  console.log(req.body);
  await categoriesModel.findByIdAndUpdate(
    { _id: idcategory },
    newCategory,
    (err, result) => {
      if (err) {
        res.send("error");
      } else {
        res.json(result);
      }
    }
  );
});

module.exports = router;
