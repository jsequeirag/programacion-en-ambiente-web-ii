/* -------------------------------------------------------------------------- */
/*                                   packages                                 */
/* -------------------------------------------------------------------------- */
require("dotenv").config();
const express = require("express");
const app = express();
const cors = require("cors");
app.use(express.json());
app.use(cors());
const mongoose = require("mongoose");

/* -------------------------------------------------------------------------- */
/*                                   Router                                   */
/* -------------------------------------------------------------------------- */
const router = require("./Router/userRouter");
const systemRouter = require("./Router/systemRouter");
const resourceRouter = require("./Router/resourceRouter");
const categoryRouter = require("./Router/categoryRouter");
const newsRouter = require("./Router/newsRouter");
app.use("/user", router);
app.use("/system", systemRouter);
app.use("/resource", resourceRouter);
app.use("/category", categoryRouter);
app.use("/news", newsRouter);
/* -------------------------------------------------------------------------- */
/*                               database connection                           */
/* -------------------------------------------------------------------------- */
const db = process.env.DB;
const user = process.env.USER;
const pass = process.env.PASS;
const uri = `mongodb+srv://${user}:${pass}@cluster0.3xs0c.mongodb.net/${db}?retryWrites=true&w=majority`;
const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
};
mongoose
  .connect(uri, options)
  .then(() => {
    console.log("DATABASE CONNECTED");
    app.listen(3001, () => {
      console.log("SERVER CONNECTED");
    });
  })
  .catch((error) => {
    throw error;
  });
