var mongoose = require("mongoose");
const { Schema } = mongoose;
const CursoSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  code: {
    type: String,
    required: true,
  },
  career: {
    type: String,
    required: true,
  },
  credits: {
    type: Number,
    required: true,
  },
});

const CursoModel = mongoose.model("cursos", CursoSchema);
module.exports = CursoModel;
