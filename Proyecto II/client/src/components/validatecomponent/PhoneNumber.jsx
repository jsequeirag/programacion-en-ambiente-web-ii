import React from "react";
import { Fragment } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import img2 from "../img/image2.svg";
export default function PhoneNumber() {
  var history = useHistory();
  const user = JSON.parse(localStorage.getItem("userInfo"));
  function emailVatidate() {
    axios
      .post("http://localhost:3001/system/phonenumbervalidate", {
        userid: user._id,
      })
      .then((res) => {
        if (res.data.phonenumbervalidate) {
          history.push("/chooseresource");
        } else {
          document.getElementById("loginspan").hidden = false;
        }
      });
  }
  function sendMessage() {
    axios
      .post("http://localhost:3001/system/sendsms", {
        phonenumber: user.phonenumber,
        userid: user._id,
      })
      .then((res) => {
        document.getElementById("sendmessagebutton").hidden = true;
        document.getElementById("numbervatidatebutton").hidden = false;
      });
  }
  return (
    <Fragment>
      <div className="loginmain row">
        <div className="col-md-4  d-flex flex-row justify-content-end align-items-center">
          <img src={img2} alt="" className="img-fluid" />
        </div>
        <div
          className="col-md-4
             d-flex flex-column justify-content-center align-items-center "
        >
          <div className="h-50">
            <div className="text-center mt-5">
              <h2>Phone number validation</h2>
              <h3>Check your phone messages to validate it</h3>{" "}
              <h4>{user.phonenumber}</h4>
              <span id="loginspan" className="text-danger" hidden>
                Check your phone messages to validate it, please
              </span>
            </div>
            <div className="text-center mt-5">
              <button
                className="loginbutton"
                id="sendmessagebutton"
                onClick={() => {
                  sendMessage();
                }}
              >
                send validation
              </button>
              <button
                className="loginbutton"
                id="numbervatidatebutton"
                hidden
                onClick={() => {
                  emailVatidate();
                }}
              >
                next
              </button>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
