import { React, useEffect, Fragment, useState } from "react";
import { useHistory } from "react-router-dom";
import img from "./img/no-image.svg";
import Loading from "./Loading";
export default function MainNews(props) {
  const history = useHistory();
  const colors = [
    "shrinkbuttonred",
    "shrinkbuttonblue",
    "shrinkbuttonyellow",
    "shrinkbuttonpurple",
    "shrinkbuttongreen",
  ];
  return props.newsList.length === 0 ? (
    <Fragment>
      <div
        className="tab-pane fade"
        id="main"
        role="tabpanel"
        aria-labelledby="ex1-tab-2"
      >
        <div className="mainframework d-flex flex-row justify-content-center align-items-center">
          <div className="divimage"></div>
          <div className="watchframework border">
            <div className="rounded">
              <h4 className="text-center m-3">Nombre de la noticia</h4>
            </div>
            <div className="buttondiv border m-1"></div>
            <div className="d-flex flex-row justify-content-center align-items-center mt-5">
              <div>
                <Loading />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  ) : (
    <Fragment>
      <div
        className="tab-pane fade"
        id="ex1-tabs-2"
        role="tabpanel"
        aria-labelledby="ex1-tab-2"
      >
        <div className="mainframework">
          <div className="scrollmainnews">
            {props.newsList.map((item, index) => (
              <div key={index} className="card m-1 d-flex flex-row  ">
                {item.image == "" ? (
                  <div className=" align-self-center">
                    <a href={item.link} target="_blank">
                      <img src={img} className="newsimage " alt="..." />
                    </a>
                  </div>
                ) : (
                  <div className=" align-self-center">
                    <a href={item.link} target="_blank">
                      <img src={item.image} className="newsimage" alt="..." />
                    </a>
                  </div>
                )}

                <div className="card-body">
                  <div className="newsmain">
                    <h3 className="text-center">{item.title}</h3>
                    <p className="card-text">{item.description}</p>
                    <a href={item.link} target="_blank">
                      <button className="shrinkbuttonskyblue mt-2 mb-2">
                        Link
                      </button>
                    </a>
                    <h4> date</h4>
                    <p className="card-text">{item.date}</p>
                    <h4>Category</h4>
                    {item.itemcategory.length === 0 ? (
                      <p className="card-text">"..."</p>
                    ) : (
                      <div className="d-flex flex-row">
                        {item.itemcategory.slice(0, 5).map((item, index) => (
                          <button
                            key={index}
                            className={` m-1 ${colors[index]}`}
                            onClick={() => {
                              history.push({
                                pathname: "/newsbycategory",
                                search: "?query=hi!",
                                state: {
                                  news: props.newsList,
                                  text: item,
                                  action: "Category",
                                },
                              });
                            }}
                          >
                            {item}
                          </button>
                        ))}
                      </div>
                    )}
                    <h4>Source</h4>
                    <p className="card-text">{item.source}</p> <h4>Creator</h4>
                    <p className="card-text">{item.creator}</p>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
}
