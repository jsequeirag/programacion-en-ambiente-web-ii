import { React, useEffect, Fragment, useState } from "react";
import Loading from "./Loading";
export default function MainNews({ props }) {
  const [newsList, setNewList] = useState([]);
  useEffect(() => {
    try {
      setNewList(props.newsList);
    } catch {
      alert("no sé");
    }
  }, []);
  return newsList.length === 0 ? (
    <Fragment>
      <div
        className="tab-pane fade"
        id="ex1-tabs-2"
        role="tabpanel"
        aria-labelledby="ex1-tab-2"
      >
        <div className="mainframework d-flex flex-row justify-content-center align-items-center">
          <div className="divimage"></div>
          <div className="watchframework border">
            <div className="rounded">
              <h4 className="text-center m-3">Nombre de la noticia</h4>
            </div>
            <div className="buttondiv border m-1"></div>
            <div className="d-flex flex-row justify-content-center align-items-center mt-5">
              <div>
                <Loading />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  ) : (
    <Fragment>
      <div
        className="tab-pane fade"
        id="ex1-tabs-2"
        role="tabpanel"
        aria-labelledby="ex1-tab-2"
      >
        <div className="watchmain d-flex flex-row justify-content-center align-items-center">
          <div className="divimage"></div>
          <div className="watchframework border">
            {newsList.length === 0 ? (
              <div>asdasd</div>
            ) : (
              <div className="scrolldiv">
                {newsList.map((item) => (
                  <div
                    key={item.content}
                    className="card m-1 d-flex flex-row  "
                  >
                    {
                      (item.image = "" ? (
                        <div className=" align-self-center">
                          <a href={item.link}>
                            <img
                              src={item.enclosure.url}
                              className="newsimage "
                              alt="..."
                            />
                          </a>
                        </div>
                      ) : (
                        <div className=" align-self-center">
                          <a href={item.link}>
                            <img
                              src={item.link}
                              className="newsimage"
                              alt="..."
                            />
                          </a>
                        </div>
                      ))
                    }

                    <div className="card-body">
                      <div className="newsmain">
                        <h3 className="text-center">{item.title}</h3>
                        <p className="card-text">{item.content}</p>
                        <a href={item.link}>
                          <button className="linkbutton">Link</button>
                        </a>
                        <h4> date</h4>
                        <p className="card-text">{item.pubDate}</p>
                        <h4>category</h4>
                        <p className="card-text">{item.categories[0]}</p>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
}
