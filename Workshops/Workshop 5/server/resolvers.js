const tasks = require("./sample");
const resolvers = {
  Query: {
    hello: () => {
      return "hello";
    },
    miNombre: (root, args) => {
      //resibe un json
      console.log(args.name);

      return `${args.name}`;
    },
    tasks: () => {
      return tasks;
    },
  },
};
module.exports = resolvers;
