import { Fragment, React, useState } from "react";
import { Formik, Form, Field } from "formik";
import { useHistory } from "react-router-dom";
import img1 from "./img/image1.svg";
import "./css/style.css";
import img4 from "./img/image4.svg";
import axios from "axios";
export default function SignUp() {
  let history = useHistory();
  const [code, editCode] = useState(0);
  const [newUser, editNewUser] = useState({});
  const sentEmail = (data) => {
    var codegenerated = Math.floor(Math.random() * (999 - 100 + 1) + 100);
    editCode(codegenerated);
    const newUser = {
      email: data.email,
      firstname: data.first,
      lastname: data.lastname,
      password: data.password,
    };
    editNewUser(newUser);
    axios
      .post("http://localhost:3001/system/sendemail", {
        email: newUser.email,
        code: codegenerated,
      })
      .then((e) => {
        console.log(e);
      });
    console.log("codigo" + codegenerated);
  };
  const onSubmit = (data) => {
    alert(JSON.stringify(data));
    sentEmail(data);
  };

  const onsubmitModal = (data) => {
    if (code == data.code) {
      axios.post("http://localhost:3001/user/signup/", newUser).then((res) => {
        if (res.data.auth) {
          localStorage.setItem("userInfo", JSON.stringify(res.data.user));
          localStorage.setItem("token", res.data.token);
          history.push("/chooseresource");
        } else {
          alert("Ooops!");
        }
      });
    } else {
      alert("Incorrect Code");
    }
  };

  var initialValues = {
    email: "",
    firstname: "",
    lastname: "",
    password: "",
  };
  var initialValuesModal = {
    code: "",
  };
  return (
    <Fragment>
      <div className="loginmain row">
        <div className="col-md-4 d-flex flex-row justify-content-end align-items-center">
          <img src={img1} alt="" className="img-fluid" />
        </div>
        <div
          className="col-md-4
         d-flex flex-row justify-content-center align-items-center "
        >
          <Formik onSubmit={onSubmit} initialValues={initialValues}>
            <Form className="form w-100  p-4">
              <div className="form-group">
                <label>Email address</label>
                <Field
                  className="form-control m-1"
                  type="email"
                  required
                  autoComplete="off"
                  name="email"
                  placeholder="example@gmail.com"
                />
                <label>First Name</label>
                <Field
                  className="form-control m-1"
                  type="text"
                  required
                  autoComplete="off"
                  name="firstname"
                  placeholder="Example:Elon "
                />
                <label>Last name</label>
                <Field
                  className="form-control m-1"
                  type="text"
                  required
                  autoComplete="off"
                  name="lastname"
                  placeholder="Example:Musk"
                />
                <label>password</label>
                <Field
                  className="form-control m-1"
                  type="password"
                  required
                  autoComplete="off"
                  name="password"
                  placeholder="Min length:6  Max Length:24 "
                />
                <div
                  className="text-center mt-5"
                  data-toggle="modal"
                  data-target="#exampleModal"
                >
                  <button
                    className="loginbutton"
                    type="submit"
                    data-toggle="modal"
                    data-target="#exampleModalCenter"
                    onClick={() => {}}
                  >
                    SignUp
                  </button>
                </div>
              </div>
            </Form>
          </Formik>

          <div
            className="modal fade"
            id="exampleModalCenter"
            tabIndex="-1"
            aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-dialog-centered">
              <div className="modal-content">
                <div className="modal-header">
                  <div className="text-center  w-100">
                    <h5 className="modal-title " id="exampleModalCenterTitle">
                      Email validation
                    </h5>
                  </div>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div className="modalemail modal-body text-center" id="modal">
                  <img src={img4} alt="" />
                  <label>Open your email and get the code</label>
                  <Formik
                    onSubmit={onsubmitModal}
                    initialValues={initialValuesModal}
                  >
                    <Form>
                      <Field
                        className="form-control"
                        type="text"
                        maxLength="3"
                        placeholder="Insert code"
                        name="code"
                      />
                      <div className="modal-footer">
                        <button
                          type="button"
                          className="btn btn-secondary"
                          data-dismiss="modal"
                        >
                          Cancel Process
                        </button>
                        <button type="submit" className=" btn-primary">
                          Confirm
                        </button>
                      </div>
                    </Form>
                  </Formik>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
