/* -------------------------------------------------------------------------- */
/*                                   package                                  */
/* -------------------------------------------------------------------------- */
const express = require("express");
const router = express.Router();
const nodemailer = require("nodemailer");
require("dotenv").config();
/* -------------------------------------------------------------------------- */
/*                              nodemailer config                             */
/* -------------------------------------------------------------------------- */
var transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: process.env.EMAIL,
    pass: process.env.EMAILPASSWORD,
  },
});

/* -------------------------------------------------------------------------- */
/*           send email-Activar siempre apliacaiones menos seguras de google  */
/* -------------------------------------------------------------------------- */
router.post("/sendemail", async (req, res) => {
  console.log("hello world");
  const email = req.body.email;
  const code = req.body.code;
  console.log(email);
  console.log(code);
  var mailOptions = {
    from: process.env.EMAIL,
    to: email,
    subject: "feed code",
    text: "" + code,
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
});
module.exports = router;
