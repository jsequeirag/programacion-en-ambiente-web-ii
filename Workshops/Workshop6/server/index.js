const express = require("express");
const cors = require("cors");
const axios = require("axios");
const jwt = require("jsonwebtoken");
const app = express();
app.use(cors());
app.use(express.json());
/* -------------------------------------------------------------------------- */
/*                               verify function                              */
/* -------------------------------------------------------------------------- */
const verifyToken = (req, res, next) => {
  const token = req.headers["x-access-token"];
  console.log(token);
  if (!token) {
    return res.status(401).json({
      auth: false,
      message: "no token provided",
    });
  }
  const decoded = jwt.verify(token, "josenator");
  req.userId = decoded.id;
  next();
};
/* -------------------------------------------------------------------------- */
/*                                     get List Breweries                      */
/* -------------------------------------------------------------------------- */

app.get("/", (req, res) => {
  const token = jwt.sign({ id: "123456" }, "josenator", {
    expiresIn: 60 * 60 * 24,
  });
  res.json({ auth: true, token });
});

app.post("/getbreweries", verifyToken, async (req, res) => {
  var response = res;
  function sendRes(res) {
    response.json(res);
  }
  await axios.get("https://api.openbrewerydb.org/breweries").then((res) => {
    sendRes(res.data);
  });
});

app.listen("3001");
console.log("CONNECTED");
