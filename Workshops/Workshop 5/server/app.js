const express = require("express");
const { graphqlHTTP } = require("express-graphql");
const schema = require("./schema");
//const { schema } = require("./schema");
const app = express();
/*app.use("/", (req, res) => {
  res.json({
    messsage: "Hello world",
  });
});*/

app.use(
  "/graphql",
  graphqlHTTP({
    graphiql: true,
    schema: schema,
  })
);

app.listen(3001, () => {
  console.log("connected");
});
