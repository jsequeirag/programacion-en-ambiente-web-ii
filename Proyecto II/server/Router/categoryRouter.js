/* -------------------------------------------------------------------------- */
/*                                   package                                  */
/* -------------------------------------------------------------------------- */
const { Router } = require("express");
const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
require("dotenv").config();
/* -------------------------------------------------------------------------- */
/*                                    Model                                   */
/* -------------------------------------------------------------------------- */
const categoriesModel = require("../Model/caterories");

/* -------------------------------------------------------------------------- */
/*                               get categories                               */
/* -------------------------------------------------------------------------- */
router.post("/getcategories", async (req, res) => {
  const categories = categoriesModel.find({}, (err, result) => {
    res.json(result);
  });
});
/* -------------------------------------------------------------------------- */
/*                               delete category                              */
/* -------------------------------------------------------------------------- */
router.post("/deletecategory", async (req, res) => {
  idCategory = req.body.idCategory;
  console.log(idCategory);
  const categories = await categoriesModel.deleteOne(
    { _id: idCategory },
    (err, result) => {
      if (err) {
        res.send("error");
      } else {
        res.send("category deleted");
      }
    }
  );
});
/* -------------------------------------------------------------------------- */
/*                                   insert                                   */
/* -------------------------------------------------------------------------- */
router.post("/insertcategory", async (req, res) => {
  const name = req.body.name;
  const categoryFound = await categoriesModel.findOne({ name: req.body.name });
  console.log(categoryFound);
  if (categoryFound) {
    return res.send({ message: "this category existed already!" });
  }

  const category = new categoriesModel({
    name: name,
  });
  await category.save();
  res.send({ auth: true, category });
});
/* -------------------------------------------------------------------------- */
/*                                   update                                   */
/* -------------------------------------------------------------------------- */
router.post("/updatecategory", async (req, res) => {
  const idcategory = req.body._id;
  const nameCategory = req.body.name;
  const newCategory = {
    _id: idcategory,
    name: nameCategory,
  };

  await categoriesModel.findByIdAndUpdate(
    { _id: idcategory },
    newCategory,
    (err, result) => {
      if (err) {
        res.send("error");
      } else {
        res.json(JSON.stringify(result));
      }
    }
  );
});

module.exports = router;
