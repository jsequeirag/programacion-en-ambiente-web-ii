import { React, Fragment, useState } from "react";
import { Formik, Form, Field } from "formik";
import { useHistory } from "react-router-dom";
import "./css/style.css";
import img2 from "./img/image2.svg";
import axios from "axios";

function LogIn() {
  let history = useHistory();

  const validateUser = (user) => {
    axios
      .post("http://localhost:3001/user/signin/", {
        email: user.email,
        password: user.password,
      })
      .then((res) => {
        if (res.data.auth) {
          console.log("datos usuario:", res.data.user);
          console.log("token:", res.data.token);
          localStorage.setItem("userInfo", JSON.stringify(res.data.user));
          localStorage.setItem("token", res.data.token);
        }

        if (
          res.data.auth &&
          res.data.user.roles[0] === "user" &&
          res.data.user.emailvalidate === false
        ) {
          history.push({
            pathname: "/emailvalidate",
            search: "?query=hi!",
          });
        } else if (
          res.data.auth &&
          res.data.user.roles[0] === "user" &&
          res.data.user.phonenumbervalidate === false
        ) {
          axios.post("http://localhost:3001/system/sendsms", {
            _id: res.data.user._id,
            number: res.data.user.number,
          });

          history.push({
            pathname: "/PhoneNumber",
            search: "?query=hi!",
          });
        } else if (
          res.data.auth &&
          res.data.user.roles[0] === "user" &&
          res.data.user.emailvalidate === true &&
          res.data.user.phonenumbervalidate === true
        ) {
          window.location.href = "/portal";
        } else if (res.data.auth && res.data.user.roles[0] === "admin") {
          window.location.href = "/admimenu";
        } else {
          alert(res.data.message);
        }
      });
  };
  var initialValues = {
    email: "",
    password: "",
  };
  const onSubmit = (data, { resetForm }) => {
    validateUser(data);
  };

  return (
    //initialValues={} onSubmit={} validationSchema={}}
    <Fragment>
      <div className="loginmain row">
        <div className="col-md-4  d-flex flex-row justify-content-end align-items-center">
          <img src={img2} alt="" className="img-fluid" />
        </div>
        <div
          className="col-md-4
             d-flex flex-row justify-content-center align-items-center "
        >
          <Formik onSubmit={onSubmit} initialValues={initialValues}>
            <Form className="form w-100 p-4">
              <label>Email address</label>
              <Field
                className="form-control m-1"
                type="email"
                required
                autoComplete="off"
                name="email"
                placeholder="Email"
              />
              <label>Password</label>
              <Field
                className="form-control m-1"
                type="password"
                required
                minLength="6"
                autoComplete="off"
                name="password"
                placeholder="Insert a security password(min 6 letter)"
              />

              <div className="text-center mt-5">
                <button className="loginbutton " type="submit">
                  log In
                </button>
              </div>
            </Form>
          </Formik>
        </div>
      </div>
    </Fragment>
  );
}

export default LogIn;
