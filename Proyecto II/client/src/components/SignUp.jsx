import { Fragment, React, useState, useEffect } from "react";
import { Formik, Form, Field } from "formik";
import { useHistory } from "react-router-dom";
import img1 from "./img/image1.svg";
import "./css/style.css";
import img4 from "./img/image4.svg";
import axios from "axios";
export default function SignUp() {
  let history = useHistory();
  const [code, editCode] = useState(0);
  const [newUser, editNewUser] = useState({});

  const onSubmit = async (data) => {
    data.roles = "user";
    alert(JSON.stringify(data));
    await axios
      .post("http://localhost:3001/system/emailconfirm", {
        email: data.email,
      })
      .then((res) => {
        if (res.data.auth) {
          axios.post("http://localhost:3001/user/signup/", data).then((res) => {
            if (res.data.auth) {
              window.location.href = "/login";
            } else {
              alert(res.data.message);
            }

            if (res.data.auth) {
              localStorage.setItem("userInfo", JSON.stringify(res.data.user));
              localStorage.setItem("token", res.data.token);
            } else {
              alert(res.data.message);
            }
          });
        } else {
          alert("email is registered");
        }
      });
  };

  var initialValues = {
    email: "",
    firstname: "",
    lastname: "",
    password: "",
    phonenumber: "",
  };

  return (
    <Fragment>
      <div className="loginmain row">
        <div className="col-md-4 d-flex flex-row justify-content-end align-items-center">
          <img src={img1} alt="" className="img-fluid" />
        </div>
        <div
          className="col-md-4
         d-flex flex-row justify-content-center align-items-center "
        >
          <Formik onSubmit={onSubmit} initialValues={initialValues}>
            <Form className="form w-100  p-4">
              <div className="form-group">
                <label>Email address</label>
                <Field
                  className="form-control m-1"
                  type="email"
                  required
                  autoComplete="off"
                  name="email"
                  placeholder="ElonMuskvsNasa@gmail.com"
                  minLength="6"
                />
                <label>First Name</label>
                <Field
                  className="form-control m-1"
                  type="text"
                  required
                  autoComplete="off"
                  name="firstname"
                  placeholder="Example:Elon "
                  minLength="2"
                />
                <label>Last name</label>
                <Field
                  className="form-control m-1"
                  type="text"
                  required
                  autoComplete="off"
                  name="lastname"
                  placeholder="Example:Musk"
                  minLength="2"
                />
                <label>phonenumber</label>
                <Field
                  className="form-control m-1"
                  type="text"
                  required
                  autoComplete="off"
                  name="phonenumber"
                  placeholder="Example:67876867"
                  minLength="6"
                />
                <label>password</label>
                <Field
                  className="form-control m-1"
                  type="password"
                  required
                  autoComplete="off"
                  name="password"
                  placeholder="Min length:6  Max Length:24 "
                  minLength="6"
                  maxLength="24"
                />
                <div
                  className="text-center mt-5"
                  data-toggle="modal"
                  data-target="#exampleModal"
                >
                  <button id="signupbutton" className="loginbutton">
                    SignUp
                  </button>
                </div>
              </div>
            </Form>
          </Formik>
        </div>
      </div>
    </Fragment>
  );
}
