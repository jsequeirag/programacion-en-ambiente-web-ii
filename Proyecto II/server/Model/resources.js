const mongoose = require("mongoose");
const resourcesSchema = new mongoose.Schema({
  userid: {
    type: String,
  },
  name: {
    type: String,
  },
  category: {
    type: String,
  },
  url: {
    type: String,
  },
});

const resourcesModel = mongoose.model("resources", resourcesSchema);
module.exports = resourcesModel;
