import { React, Fragment } from "react";
import { Formik, Form, Field } from "formik";
import { useHistory } from "react-router-dom";
import "./css/style.css";
import img2 from "./img/image2.svg";
import axios from "axios";

function LogIn() {
  let history = useHistory();
  const validateUser = (user) => {
    if (user.email === "admi@admi.com" && user.password === "123456") {
      history.push("/admimenu");
    } else {
      axios
        .post("http://localhost:3001/user/login/", {
          email: user.email,
          password: user.password,
        })
        .then((res) => {
          if (res.data.auth) {
            history.push("/portal");
            console.log("datos usuario:", res.data.user);
            console.log("token:", res.data.token);
            localStorage.setItem("userInfo", JSON.stringify(res.data.user));
            localStorage.setItem("token", res.data.token);
          } else {
            alert("Ooops!");
          }
        });
    }
  };
  var initialValues = {
    email: "",
    password: "",
  };
  const onSubmit = (data, { resetForm }) => {
    validateUser(data);
  };

  return (
    //initialValues={} onSubmit={} validationSchema={}}
    <Fragment>
      <div className="loginmain row">
        <div className="col-md-4  d-flex flex-row justify-content-end align-items-center">
          <img src={img2} alt="" className="img-fluid" />
        </div>
        <div
          className="col-md-4
             d-flex flex-row justify-content-center align-items-center "
        >
          <Formik onSubmit={onSubmit} initialValues={initialValues}>
            <Form className="form w-100 p-4">
              <label>Email address</label>
              <Field
                className="form-control m-1"
                type="email"
                required
                autoComplete="off"
                name="email"
                placeholder="Email"
              />
              <label>Password</label>
              <Field
                className="form-control m-1"
                type="password"
                required
                minLength="6"
                autoComplete="off"
                name="password"
                placeholder="Insert a security password(min 6 letter)"
              />

              <div className="text-center mt-5">
                <button className="loginbutton " type="submit">
                  log In
                </button>
              </div>
            </Form>
          </Formik>
        </div>
      </div>
    </Fragment>
  );
}

export default LogIn;
