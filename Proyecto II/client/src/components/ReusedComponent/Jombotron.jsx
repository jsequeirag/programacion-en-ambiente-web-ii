import { React, Fragment } from "react";
import "./css/Jombotron.css";
import indeximage from "./img/indeximage.svg";
export default function Jombotron() {
  return (
    <Fragment>
      <div className="jumbotron d-flex flex-column justify-content-center align-items-center">
        <div className="img ">
          <img src={indeximage} className="img-fluid" width="550px" alt="" />
        </div>
        <h2 className="m-3">ALL TIME INFORMED!</h2>
        <a href="/signup">
          <button className="indexbutton p-2 m-3">JOIN ME</button>
        </a>
      </div>
    </Fragment>
  );
}
