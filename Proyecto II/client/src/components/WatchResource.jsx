import { useEffect, Fragment } from "react";
import { React, useState } from "react";
import { useLocation } from "react-router-dom";
import { useHistory } from "react-router-dom";
import img from "./img/no-image.svg";
import Loading from "./Loading";
export default function WatchResource(props) {
  const colors = [
    "shrinkbuttonred",
    "shrinkbuttonblue",
    "shrinkbuttonyellow",
    "shrinkbuttonpurple",
    "shrinkbuttongreen",
  ];
  var history = useHistory();
  const location = useLocation();
  const [news, setNews] = useState([]);

  useEffect(() => {
    const newFilter = location.state.news.filter((item) => {
      return item.source === location.state.resourcename;
    });
    setNews(newFilter);
  }, []);

  return news.length === 0 ? (
    <div className="watchmain d-flex flex-row justify-content-center align-items-center">
      <div className="divimage"></div>
      <div className="watchframework border">
        <div className="rounded">
          <h4 className="text-center m-3">Nombre de la noticia</h4>
        </div>
        <div className="buttondiv border m-1">
          <button
            className="backbutton text-center m-1"
            onClick={() => {
              history.push("/portal");
            }}
          >
            Back
          </button>
        </div>
        <div className="d-flex flex-row justify-content-center align-items-center mt-5">
          <Loading />
        </div>
      </div>
    </div>
  ) : (
    <Fragment>
      <div className="watchmain d-flex flex-row justify-content-center align-items-center">
        <div className="divimage"></div>
        <div className="watchframework border">
          <div className="rounded">
            <h4 className="text-center m-3">{location.state.resourcename}</h4>
          </div>
          <div className="buttondiv border m-1">
            <button
              className="backbutton text-center m-1"
              onClick={() => {
                history.push("/portal");
              }}
            >
              Back
            </button>
          </div>
          <div className="scrolldiv">
            {news.map((item, index) => (
              <div key={index} className="card m-1 d-flex flex-row  ">
                {item.image == "" ? (
                  <div className=" align-self-center">
                    <a href={item.link} target="_blank">
                      <img src={img} className="newsimage " alt="..." />
                    </a>
                  </div>
                ) : (
                  <div className=" align-self-center">
                    <a href={item.link} target="_blank">
                      <img src={item.image} className="newsimage" alt="..." />
                    </a>
                  </div>
                )}

                <div className="card-body">
                  <div className="newsmain">
                    <h3 className="text-center">{item.title}</h3>
                    <p className="card-text">{item.description}</p>
                    <a href={item.link}>
                      <button className="shrinkbuttonskyblue mt-2 mb-2">
                        Link
                      </button>
                    </a>
                    <h4> date</h4>
                    <p className="card-text">{item.date}</p>
                    <h4>Category</h4>
                    {item.itemcategory.length === 0 ? (
                      <p className="card-text">"..."</p>
                    ) : (
                      <div className="d-flex flex-row">
                        {item.itemcategory.slice(0, 5).map((item, index) => (
                          <button
                            key={index}
                            className={` m-1 ${colors[index]}`}
                            onClick={() => {
                              history.push({
                                pathname: "/newsbycategory",
                                search: "?query=hi!",
                                state: {
                                  news: props.newsList,
                                  text: item,
                                  action: "Category",
                                },
                              });
                            }}
                          >
                            {item}
                          </button>
                        ))}
                      </div>
                    )}
                    <h4>Source</h4>
                    <p className="card-text">{item.source}</p> <h4>Creator</h4>
                    <p className="card-text">{item.creator}</p>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
}
