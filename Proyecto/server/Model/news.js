const mongoose = require("mongoose");
const newsSchema = new mongoose.Schema({
  userid: {
    type: String,
  },
  title: {
    type: String,
  },
  description: {
    type: String,
  },
  source: {
    type: String,
  },
  creator: {
    type: String,
  },
  link: {
    type: String,
  },
  itemcategory: {
    type: String,
  },
  resourcecategory: {
    type: String,
  },
  image: {
    type: String,
  },
  date: {
    type: Date,
  },
});

const newsModel = mongoose.model("news", newsSchema);
module.exports = newsModel;
