import { Fragment, React, useState, useEffect } from "react";
import axios from "axios";
import { Formik, Form, Field } from "formik";
import { useHistory } from "react-router-dom";
import bbcico from "./ico/bbcico.ico";
import elpaisico from "./ico/elpaisico.ico";
import img3 from "./img/image3.svg";
import "./css/style.css";
export default function ChooseResource() {
  useEffect(() => {
    document.body.classList.remove("modal-open");
    const div = document.getElementsByClassName("modal-backdrop");
    div.forEach((element) => {
      element.parentNode.removeChild(element);
    });
  }, []);
  let history = useHistory();
  const [boolean, changeBoolean] = useState(true);
  const [index, changeIndex] = useState(1000);

  function changeBooleanFunc() {
    if (index === 1000) {
      var text = document.getElementById("textAdvice");
      text.removeAttribute("hidden");
    } else {
      changeBoolean(false);
    }
  }
  function onSubmit() {
    const token = localStorage.getItem("token");
    const user = localStorage.getItem("userInfo");
    const jsonUser = JSON.parse(user);
    const newResource = {
      userid: jsonUser._id,
      name: rrss[index].name,
      category: rrss[index].category,
      url: rrss[index].url,
    };
    let config = {
      headers: {
        "x-access-token": token,
      },
    };
    axios
      .post("http://localhost:3001/resource/saveresource/", newResource, config)
      .then((r) => {
        history.push("/portal");
      });
  }
  const rrss = [
    {
      name: "BBC NEWS",
      category: "General",
      url: "http://www.bbc.co.uk/mundo/index.xml",
    },
    {
      name: "ELPAIS",
      category: "General",
      url: "https://feeds.elpais.com/mrss-s/pages/ep/site/elpais.com/portada",
    },
  ];
  function changeIndexFunc(param) {
    changeIndex(param);
  }
  function skip() {
    history.push("/portal");
  }
  var initialValues = {
    name: "",
    category: "",
    url: "",
  };
  return boolean ? (
    <Fragment>
      <div className="chooseresourcemain row">
        <div className="col-md-4 d-flex flex-row justify-content-end align-items-center">
          <img src={img3} alt="" className="img-fluid" />
        </div>
        <div className="col-md-4 d-flex flex-column justify-content-center align-items-center mb-3">
          <div className="chooseframework  border p-5 w-100">
            <h1 className="mb-5 text-center">Hi Jose!</h1>
            <h2 className="mb-5 text-center ">Choose a resource</h2>
            <p id="textAdvice" className="text-danger text-center" hidden>
              Choose an option, please!
            </p>

            <div className="form-check d-flex flex-row  align-items-center ">
              <input
                className="form-check-input m-2"
                type="radio"
                name="flexRadioDefault"
                id="flexRadioDefault1"
                onClick={() => {
                  changeIndexFunc(0);
                }}
              />
              <label className="form-check-label m-4" for="flexRadioDefault1">
                BBC NEWS
              </label>
              <div className="">
                <img src={bbcico} alt="" width="50px" height="50px" />
              </div>
            </div>
            <div className="form-check d-flex flex-row  align-items-center mb-5">
              <input
                className="form-check-input m-2"
                type="radio"
                name="flexRadioDefault"
                id="flexRadioDefault1"
                onClick={() => {
                  changeIndexFunc(1);
                }}
              />
              <label className="form-check-label  m-4" for="flexRadioDefault1">
                ELPAIS
              </label>
              <div className="">
                <img src={elpaisico} alt="" width="30px" height="30px" />
              </div>
            </div>
            <div className="d-flex d-row flex-row justify-content-center">
              <button
                className=" basicbutton mr-2"
                onClick={() => {
                  history.push("/portal");
                }}
              >
                Skip
              </button>
              <button
                className="basicbutton ml-2"
                onClick={() => {
                  changeBooleanFunc();
                }}
              >
                Next
              </button>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  ) : (
    <div className="chooseresourcemain row">
      <div className="col-md-4 d-flex flex-row justify-content-end align-items-center">
        <img src={img3} alt="" className="img-fluid" />
      </div>
      <div className="col-md-4 d-flex flex-column justify-content-center align-items-center ">
        <div className="chooseframework border w-100 p-5">
          <h1 className="text-center mb-5">Info required</h1>
          <Formik onSubmit={onSubmit} initialValues={initialValues}>
            <Form className="w-100 p-4 ">
              <label>Name</label>
              <Field
                disabled
                className="form-control m-1"
                type="text"
                required
                autoComplete="off"
                name="email"
                value={rrss[index].name}
              />
              <label>Category</label>
              <Field
                disabled
                type="text"
                className="form-control m-1"
                value={rrss[index].category}
              />
              <label>url</label>
              <Field
                disabled
                type="text"
                className="form-control m-1"
                value={rrss[index].url}
              />
              <div className="text-center  mt-5">
                <button
                  className="loginbutton mr-2"
                  onClick={() => {
                    changeBoolean(true);
                    changeIndex(1000);
                  }}
                >
                  Back
                </button>
                <button className="loginbutton ml-2" type="submit">
                  Save
                </button>
              </div>
            </Form>
          </Formik>
        </div>
      </div>
    </div>
  );
}
