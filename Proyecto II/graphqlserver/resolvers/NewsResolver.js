const newsModel = require("../models/NewsModel");
const resolvers = {
  Query: {
    getNews: (_, { userid }) => {
      const news = newsModel.find({ userid }).sort({ date: -1 });
      return news;
    },
  },
};
module.exports = resolvers;
