const { makeExecutableSchema } = require("graphql-tools");
const resolvers = require("../resolvers/NewsResolver");

const typeDefs = `
scalar DateTime
type Query {
    getNews(userid: String):[News]
    }
    type News{
        userid:String,
        resourceurl:String,
        title:String,
        description:String,
        source:String,
        creator:String,
        link:String,
        itemcategory:[String],
        resourcecategory:String,
        image:String,
        date:DateTime
    }`;

module.exports = makeExecutableSchema({
  typeDefs,
  resolvers,
});
