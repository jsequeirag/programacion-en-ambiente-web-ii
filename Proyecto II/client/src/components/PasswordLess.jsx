import { React, useEffect } from "react";
import { useLocation } from "react-router-dom";
import axios from "axios";
export default function PasswordLess() {
  const location = useLocation();
  const query = location.search;
  useEffect(() => {
    axios
      .post("http://localhost:3001/user/getuserbypasscode", {
        passwordlesscode: query.substring(1),
      })
      .then((res) => {
        if (res.data.auth === true && res.data.user.passwordless === true) {
          localStorage.setItem("userInfo", JSON.stringify(res.data.user));
          localStorage.setItem("token", res.data.token);
          window.location.href = "/portal";
        } else {
          window.location.href = "/";
        }
      });
  }, []);

  console.log(query);
  return <div>Validating...</div>;
}
