import React from "react";

export default function Table(props) {
  console.log(props.cambios);
  return (
    <div className="container ">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Tipo de cambio</th>
            <th scope="col">Valor</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          {props.cambios.map((cambio, index) => (
            <tr>
              <td>{cambio.tipo_cambio}</td>
              <td>{cambio.valor}</td>
              <button
                onClick
                onClick={() => {
                  props.update(cambio._id);
                }}
                className="mt-1 btn button btn-warning "
              >
                Editar
              </button>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
