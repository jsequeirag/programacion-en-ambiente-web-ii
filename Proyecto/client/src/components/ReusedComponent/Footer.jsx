import React from "react";
import "./css/Footer.css";
import linkedicon from "./img/linkedicon.ico";
export default function Footer() {
  return (
    <div className="footer border d-flex flex-column justify-content-center align-items-center ">
      <a href="https://www.facebook.com/" className="mt-3">
        <img src={linkedicon} alt="" />
      </a>
      <p>Jose Luis Sequeira Gongora &copy; 2021</p>
    </div>
  );
}
