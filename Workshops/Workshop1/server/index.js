const express = require("express");
const app = express();
const cors = require("cors");
const mongoose = require("mongoose");
const TipoCambioModel = require("./model/Cambio");
const CambioModel = require("./model/Cambio");
app.use(cors());
app.use(express.json());

/* -------------------------------------------------------------------------- */
/*                             conexion base datos                            */
/* -------------------------------------------------------------------------- */
const user = "MongoDB";
const password = "2141996JoSe";
const database = "cambio";
const uri = `mongodb+srv://${user}:${password}@cluster0.3xs0c.mongodb.net/${database}?retryWrites=true&w=majority`;
mongoose
  .connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("Base de Datos conectada"))
  .catch((e) => console.log(e));

/* -------------------------------------------------------------------------- */
/*                                    leer                                    */
/* -------------------------------------------------------------------------- */

app.get("/read", async (req, res) => {
  TipoCambioModel.find({}, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
      console.log(result);
    }
  });
});
/* -------------------------------------------------------------------------- */
/*                                   update                                   */
/* -------------------------------------------------------------------------- */

app.put("/update", (req, res) => {
  id = req.body.id;
  tipoCambio = req.body.tipoCambio;
  valor = req.body.valor;
  console.log("id:" + id + "tcambio" + tipoCambio, "tipovalor" + valor);
  CambioModel.findById(id, (error, cambioToUpdate) => {
    try {
      cambioToUpdate.tipo_cambio = String(tipoCambio);
      cambioToUpdate.valor = Number(valor);
      cambioToUpdate.save();
      res.send("updated:" + cambioToUpdate);
    } catch (error) {
      console.log("ERROR CONSOLE:" + error);
      res.send("ERROR CONSOLE:" + error);
    }
  });
});
/* -------------------------------------------------------------------------- */
/*                                   insert                                   */
/* -------------------------------------------------------------------------- */
app.post("/addcambio", (req, res) => {
  const tipoCambio = req.body.tipoCambio;
  const valor = req.body.valor;
  const cambio = new CambioModel({
    tipo_cambio: tipoCambio,
    valor: valor,
  });
  cambio.save();
  res.send(cambio);
});

const port = 3001;
app.listen(port, () => {
  console.log("You are connected:" + port);
});
