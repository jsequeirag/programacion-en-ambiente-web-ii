import React, { useState, Fragment, useEffect } from "react";
import Table from "./components/Table";
import Insert from "./components/Insert";
import Axios from "axios";
function App() {
  const [cambios, setCambios] = useState([]);
  /* -------------------------------------------------------------------------- */
  /*                                   insert                                   */
  /* -------------------------------------------------------------------------- */
  const addCambio = () => {
    const tipoCambio = prompt("Digite el tipo cambio");
    const valor = prompt("Digite el valor");
    Axios.post("http://localhost:3001/addcambio", {
      tipoCambio: tipoCambio,
      valor: valor,
    })
      .then((response) => {
        const id = response.data.id;
        const tipoCambio = response.data.tipo_cambio;
        const valor = response.data.valor;
        setCambios([
          ...cambios,
          {
            _id: id,
            tipo_cambio: tipoCambio,
            valor: valor,
          },
        ]);
        console.log(response);
        alert("registed:" + JSON.stringify(response.data));
      })
      .catch((err) => {
        alert(err);
      });
  };
  /* -------------------------------------------------------------------------- */
  /*                                   update                                   */
  /* -------------------------------------------------------------------------- */
  const update = (id) => {
    console.log(id);
    const cambio = prompt("Digite el tipo de cambio");
    const valor = prompt("Digite el valor del cambio");
    Axios.put("http://localhost:3001/update", {
      id: id,
      tipoCambio: cambio,
      valor: valor,
    })
      .then((response) => {
        setCambios(
          cambios.map((paramcambio) => {
            return paramcambio._id === id
              ? { _id: id, tipo_cambio: cambio, valor: valor }
              : paramcambio;
          })
        );
        console.log(response);
        alert(response.data);
      })
      .catch((response) => {
        console.log(response);
        alert(response.data);
      });
  };
  /* -------------------------------------------------------------------------- */
  /*                                    read                                    */
  /* -------------------------------------------------------------------------- */
  useEffect(() => {
    Axios.get("http://localhost:3001/read")
      .then((response) => {
        setCambios(response.data);
      })
      .catch((response) => {
        console.log(response);
      });
  }, []);
  return (
    <Fragment>
      <Insert addCambio={addCambio} />
      <Table cambios={cambios} update={update} />
    </Fragment>
  );
}

export default App;
