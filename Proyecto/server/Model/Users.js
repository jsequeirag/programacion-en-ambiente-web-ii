var mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const { Schema } = mongoose;
const usersSchema = new mongoose.Schema({
  email: {
    type: String,
  },
  firstname: {
    type: String,
  },
  lastname: {
    type: String,
  },
  password: {
    type: String,
  },
});

usersSchema.methods.encryptPassword = async (password) => {
  const salt = bcrypt.genSaltSync(10);
  return bcrypt.hashSync(password, salt);
};

usersSchema.methods.comparePassword = function (password) {
  return bcrypt.compare(password, this.password);
};

const usersModel = mongoose.model("users", usersSchema);
module.exports = usersModel;
