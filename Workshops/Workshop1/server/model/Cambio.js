var mongoose = require("mongoose");
const { Schema } = mongoose;
const CambioSchema = new mongoose.Schema({
  tipo_cambio: {
    type: String,
    required: true,
  },
  valor: {
    type: Number,
    required: true,
  },
});

const CambioModel = mongoose.model("cambio", CambioSchema);
module.exports = CambioModel;
