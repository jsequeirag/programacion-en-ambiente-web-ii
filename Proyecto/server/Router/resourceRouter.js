/* -------------------------------------------------------------------------- */
/*                                   package                                  */
/* -------------------------------------------------------------------------- */
const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
require("dotenv").config();
/* -------------------------------------------------------------------------- */
/*                                vefify token                                */
/* -------------------------------------------------------------------------- */
const verifyToken = (req, res, next) => {
  const token = req.headers["x-access-token"];
  if (!token) {
    return res.status(401).json({
      auth: false,
      message: "no token provided",
    });
  }
  const decoded = jwt.verify(token, process.env.TOKENACCESS);
  console.log("token", decoded);
  next();
};
/* -------------------------------------------------------------------------- */
/*                                    Model                                   */
/* -------------------------------------------------------------------------- */
const resourcesModel = require("../Model/resources");
/* -------------------------------------------------------------------------- */
/*                                save resource                               */
/* -------------------------------------------------------------------------- */
router.post("/saveresource", verifyToken, async (req, res, next) => {
  const { userid, name, category, url } = await req.body;
  console.log(userid, name, category, url);
  const modelo = new resourcesModel({
    userid: userid,
    name: name,
    category: category,
    url: url,
  });

  modelo.save();
  res.send(req.body);
});

/* -------------------------------------------------------------------------- */
/*                                get resources                               */
/* -------------------------------------------------------------------------- */
router.post("/getresources", verifyToken, async (req, res) => {
  const { userid } = req.body;
  const resources = resourcesModel.find({ userid: userid }, (err, result) => {
    res.json(result);
  });
});

/* -------------------------------------------------------------------------- */
/*                              delete resources                              */
/* -------------------------------------------------------------------------- */
router.post("/deleteresource", verifyToken, async (req, res) => {
  var idResource = req.body.idResource;
  console.log(idResource);
  resource = resourcesModel.deleteOne({ _id: idResource }, (err, result) => {
    res.send("items:", idResource, "deleted");
  });
});

/* -------------------------------------------------------------------------- */
/*                               update resource                              */
/* -------------------------------------------------------------------------- */
router.post("/updateresource", verifyToken, async (req, res) => {
  const idResource = req.body._id;
  console.log("id" + idResource);
  const resource = {
    name: req.body.name,
    category: req.body.category,
    url: req.body.url,
  };
  await resourcesModel.findByIdAndUpdate(
    { _id: idResource },
    resource,
    (err, result) => {
      if (err) {
        res.send(err);
      } else {
        res.json("changed!");
      }
    }
  );
});
module.exports = router;
