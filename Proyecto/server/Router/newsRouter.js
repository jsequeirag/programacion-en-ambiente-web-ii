/* -------------------------------------------------------------------------- */
/*                                   package                                  */
/* -------------------------------------------------------------------------- */
const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
require("dotenv").config();
const axios = require("axios");
let Parser = require("rss-parser");

/* -------------------------------------------------------------------------- */
/*                                vefify token                                */
/* -------------------------------------------------------------------------- */
const verifyToken = (req, res, next) => {
  const token = req.headers["x-access-token"];
  if (!token) {
    return res.status(401).json({
      auth: false,
      message: "no token provided",
    });
  }
  const decoded = jwt.verify(token, process.env.TOKENACCESS);
  console.log("token", decoded);
  next();
};
/* -------------------------------------------------------------------------- */
/*                                    model                                   */
/* -------------------------------------------------------------------------- */
const newsModel = require("../Model/news");
const resourcesModel = require("../Model/resources");
/* -------------------------------------------------------------------------- */
/*                                load resource                               */
/* -------------------------------------------------------------------------- */
router.post("/loadresource", verifyToken, async (req, res) => {
  const url = req.body.url;
  let parser = new Parser();
  (async () => {
    let feed = await parser.parseURL(url);
    res.send(feed.items);
  })();
});
/* -------------------------------------------------------------------------- */
/*                             get user resources                             */
/* -------------------------------------------------------------------------- */
const getUserResources = async (req, res, next) => {
  var id = req.body.userid;
  const resources = resourcesModel.find({ userid: id }, async (err, result) => {
    if (err) {
      res.send(err);
    } else {
      var userResources = result;
      req.resource = { userid: id, userResources: userResources };
      next();
    }
  });
};
/* -------------------------------------------------------------------------- */
/*                           load news to database by id                      */
/* -------------------------------------------------------------------------- */
router.post("/loadnewstodatabase", getUserResources, async (req, res) => {
  var userResources = req.resource.userResources;
  var userId = req.resource.userId;
  const loadresources = async () => {
    await userResources.map((userResources) => {
      let parser = new Parser();
      (async () => {
        let feed = await parser.parseURL(userResources.url);
        feed.items.map((item) => {
          var image;
          var itemCategory;
          var creator;
          if (typeof item.enclosure === "undefined") {
            image = "";
          } else {
            image = item.enclosure.url;
          }
          if (typeof item.categories === "undefined") {
            itemCategory = "";
          } else {
            itemCategory = item.categories[0];
          }
          if (typeof item.creator === "undefined") {
            creator = "";
          } else {
            creator = item.creator;
          }
          const news = newsModel({
            userid: userResources.userid,
            title: item.title,
            description: item.content,
            source: userResources.name,
            creator: creator,
            link: item.link,
            itemcategory: itemCategory,
            resourcecategory: userResources.category,
            image: image,
            date: item.isoDate,
          });
          news.save();
        });
      })();
    });
    res.send("database updated");
  };
  loadresources();
});

/* -------------------------------------------------------------------------- */
/*                            delete news by userId                           */
/* -------------------------------------------------------------------------- */
router.post("/deletenewsbyid", async (req, res) => {
  const userId = req.body.userid;
  const news = newsModel.deleteMany({ userid: userId }, (err, result) => {
    res.send("news deleted");
  });
});

/* -------------------------------------------------------------------------- */
/*                             get news by userid                             */
/* -------------------------------------------------------------------------- */
router.post("/getnewsbyuserid", async (req, res) => {
  const userid = req.body.userid;
  const news = await newsModel
    .find({ userid: userid }, (err, result) => {
      if (err) {
        res.send(err);
      } else {
        res.json(result);
      }
    })
    .sort({ date: -1 });
});

/* -------------------------------------------------------------------------- */
/*                               update database                              */
/* -------------------------------------------------------------------------- */
router.post("/updatedatabasebyid", getUserResources, async (req, res) => {
  var userResources = req.resource.userResources;
  var userId = req.resource.userid;
  console.log("id de usuario:", userId);
  const news = await newsModel.deleteMany({ userid: userId }, (err, result) => {
    if (err) {
      res.send("error al actualizar la base de datos");
    } else {
      console.log("se borrarón las noticias");
    }
  });
  await userResources.map((userResources, index) => {
    let parser = new Parser();

    (async () => {
      let feed = await parser.parseURL(userResources.url);
      console.log("item", index);
      console.log("numero de noticias", feed.items.length);

      feed.items.map((item) => {
        var image;
        var itemCategory;
        if (typeof item.enclosure === "undefined") {
          image = "";
        } else {
          image = item.enclosure.url;
        }
        if (typeof item.categories === "undefined") {
          itemCategory = "";
        } else {
          itemCategory = item.categories[0];
        }
        if (typeof item.creator === "undefined") {
          creator = "";
        } else {
          creator = item.creator;
        }
        const news = newsModel({
          userid: userResources.userid,
          title: item.title,
          description: item.content,
          source: userResources.name,
          creator: creator,
          link: item.link,
          itemcategory: itemCategory,
          resourcecategory: userResources.category,
          image: image,
          date: item.isoDate,
        });
        news.save();
      });
    })();
  });
  res.send("database updated");
});

module.exports = router;
