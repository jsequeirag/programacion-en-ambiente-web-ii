/* -------------------------------------------------------------------------- */
/*                                  packages                                  */
/* -------------------------------------------------------------------------- */
const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
require("dotenv").config();
/* -------------------------------------------------------------------------- */
/*                                   models                                   */
/* -------------------------------------------------------------------------- */
const usersModel = require("../Model/users");

const { get } = require("mongoose");
/* -------------------------------------------------------------------------- */
/*                                    login                                   */
/* -------------------------------------------------------------------------- */
router.post("/login", async (req, res) => {
  const { email, password } = req.body;

  /* ----------------- busca a un usuario por medio del email ----------------- */
  const user = await usersModel.findOne({ email: email });
  console.log("usuario encontrado con el email:", user);
  if (!user) {
    return res.status(404).send("the email doesn exist");
  }
  /* -------- compara las claves del usuario encontrado con el recibido ------- */
  const passwordValidate = await user.comparePassword(password);
  console.log("valor de  passwordValidate", passwordValidate);
  if (!passwordValidate) {
    return res.status(404).send("the password doesn exist");
  }
  /* ---------------------------- se crea el token ---------------------------- */
  const token = jwt.sign({ id: user._id }, process.env.TOKENACCESS, {
    expiresIn: 60 * 60 * 24,
  });
  /* ------------------ se envia datos del usuario y el token ----------------- */
  console.log("Token access", token);
  user.password = "...";
  res.send({ auth: true, token, user });
});

/* -------------------------------------------------------------------------- */
/*                                    signup                                   */
/* -------------------------------------------------------------------------- */
router.post("/signup", async (req, res) => {
  const { email, firstname, lastname, password } = req.body;
  /* ---------------------------- se crea el modelo --------------------------- */
  const user = new usersModel({
    email: email,
    firstname: firstname,
    lastname: lastname,
    password: password,
  });
  /* -------------------------- la clave se encripta -------------------------- */
  user.password = await user.encryptPassword(user.password);

  /* ----------------- se salva el usuario en la base de datos ---------------- */
  user.save();

  /* ---------------------------- se crea el token ---------------------------- */
  const token = jwt.sign({ id: user._id }, process.env.TOKENACCESS, {
    expiresIn: 60 * 60 * 24,
  });

  /* ---------------------------- se envia el token --------------------------- */
  res.send({ auth: true, token, user });
});

/* -------------------------------------------------------------------------- */
/*                                save resource                               */
/* -------------------------------------------------------------------------- */

module.exports = router;
