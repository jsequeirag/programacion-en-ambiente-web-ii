import { React, Fragment, useEffect } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import img2 from "../img/image2.svg";

export default function EmailValidate() {
  var history = useHistory();
  const user = JSON.parse(localStorage.getItem("userInfo"));
  function emailVatidate() {
    axios
      .post("http://localhost:3001/system/emailvalidate", {
        email: user.email,
        userid: user._id,
      })
      .then((res) => {
        if (res.data.emailvalidate) {
          history.push("/PhoneNumber");
        } else {
          document.getElementById("loginspan").hidden = false;
        }
      });
  }
  function sendEmail() {
    axios
      .post("http://localhost:3001/system/sendemail", {
        email: user.email,
        userid: user._id,
      })
      .then((res) => {
        document.getElementById("emailvatidatebutton").hidden = false;
        document.getElementById("sendemailbutton").hidden = true;
      });
  }

  return (
    <Fragment>
      <div className="loginmain row">
        <div className="col-md-4  d-flex flex-row justify-content-end align-items-center">
          <img src={img2} alt="" className="img-fluid" />
        </div>
        <div
          className="col-md-4
             d-flex flex-column justify-content-center align-items-center "
        >
          <div className="h-50">
            <div className="text-center mt-5">
              {" "}
              <h2>Email validation</h2>
              <h3>Check your email to validate it</h3> <h4>{user.email}</h4>
              <span id="loginspan" className="text-danger" hidden>
                validate your email, please
              </span>
            </div>
            <div className="text-center mt-5">
              <button
                className="loginbutton"
                id="sendemailbutton"
                type="submit"
                onClick={() => {
                  sendEmail();
                }}
              >
                send validation
              </button>
              <button
                className="loginbutton"
                id="emailvatidatebutton"
                type="submit"
                hidden
                onClick={() => {
                  emailVatidate();
                }}
              >
                next
              </button>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
