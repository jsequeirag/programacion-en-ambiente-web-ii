import { React, Fragment, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import img6 from "./img/image6.svg";
import img7 from "./img/image7.svg";
import { Formik, Form, Field } from "formik";
export default function NewSource() {
  var history = useHistory();
  const [categories, setCategories] = useState([]);
  /* -------------------------------------------------------------------------- */
  /*                               get categories                               */
  /* -------------------------------------------------------------------------- */

  useEffect(() => {
    axios
      .post("http://localhost:3001/category/getcategories")
      .then((response) => {
        setCategories(response.data);
      });
  }, []);
  /* ------------------------------------  ----------------------------------- */

  const initialValues = {
    name: "",
    category:"",
    url: "",
  };
  /* -------------------------------------------------------------------------- */
  /*                                save resource                               */
  /* -------------------------------------------------------------------------- */
  function onSubmit(data) {
    console.log(data)
    const token = localStorage.getItem("token");
    const user = localStorage.getItem("userInfo");
    const jsonUser = JSON.parse(user);
    const userid = jsonUser._id;
    const newResource = {
      userid: userid,
      name: data.name,
      category: data.category,
      url: data.url,
    };
    let config = {
      headers: {
        "x-access-token": token,
      },
    };

    axios
      .post("http://localhost:3001/resource/saveresource", newResource, config)
      .then((response) => {
        console.log("respuesta:", response.data);
         history.push("/portal");
      });
  }
  return (
    <Fragment>
      <div className="newresourcemain row">
        <div className="col-md-4 d-flex flex-row justify-content-end align-items-center ">
          <img src={img6} alt="" className="img-fluid" />
        </div>
        <div className="col-md-4 d-flex flex-row justify-content-center align-items-center ">
          <Formik onSubmit={onSubmit} initialValues={initialValues}>
            <Form className="form w-100  p-4">
              <div className="form-group">
                <label>Name</label>
                <Field
                  className="form-control m-1"
                  type="text"
                  required
                  autoComplete="off"
                  name="name"
                  placeholder="bbc"
                />
                <label>Category</label>
                <Field class="form-control m-1" as="select" name="category">
                <option>  </option>
                  {categories.map((category) => (
                    <option key={category._id} value={category.name}>
                      {category.name}
                    </option>
                  ))}
                </Field>
                <label>url</label>
                <Field
                  className="form-control m-1"
                  type="url"
                  required
                  autoComplete="off"
                  name="url"
                  placeholder="http://www.bbc.co.uk/mundo/index.xml"
                />
                <div className="text-center mt-5">
                  <button className="savebutton" type="submit ">
                    Save
                  </button>
                </div>
              </div>
            </Form>
          </Formik>
        </div>
      </div>
    </Fragment>
  );
}
